from typing import Any, Dict, List, Optional

from kaitai_models.serializable import Serializable


class KaitaiDocAttr(Serializable):
    def __init__(
        self,
        path: List[str] = [],
        summary: Optional[str] = None,
        refs: List[str] = None,
    ) -> None:
        super().__init__(path)
        self.summary: Optional[str] = summary
        self.refs: List[str] = refs

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.summary = data.get("doc")
        self.refs = [str(x) for x in data.get("doc-ref", [])]

    def serialize(self) -> Dict[str, Any]:
        o = {}
        if self.summary is not None:
            o["doc"] = self.summary
        if len(self.refs) > 0:
            o["doc-ref"] = self.refs
        return o
