from typing import Any, Dict, List, Optional

from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.member import KaitaiMember


class KaitaiAttr(KaitaiMember):
    def __init__(
        self,
        path: List[str] = [],
        index: Optional[int] = None,
        id: Optional[str] = None,
        type: Optional[str] = None,
        process: Optional[Any] = None,
        contents: Optional[bytes] = None,
        doc: Optional[KaitaiDocAttr] = None,
        size: Optional[int | str] = None,
        size_eos: bool = False,
        if_expr: Optional[str] = None,
        encoding: Optional[str] = None,
        terminator: Optional[int] = None,
        eos_error: bool = True,
        pad_right: Optional[int] = None,
        enum: Optional[str] = None,
        parent: Optional[str] = None,
        valid: Optional[Any] = None,
        repeat: Optional[str] = None,
        repeat_expr: Optional[str] = None,
        repeat_until: Optional[str] = None,
    ) -> None:
        super().__init__(path, id, type, enum, doc)
        self.index: Optional[int] = index
        self.process: Optional[Any] = process
        self.contents: Optional[bytes] = contents
        self.size: Optional[int | str] = size
        self.size_eos: bool = size_eos
        self.if_expr: Optional[str] = if_expr
        self.encoding: Optional[str] = encoding
        self.terminator: Optional[int] = terminator
        self.eos_error: bool = eos_error
        self.pad_right: Optional[int] = pad_right
        self.parent: Optional[str] = parent
        self.valid: Optional[Any] = valid
        self.repeat: Optional[str] = repeat
        self.repeat_expr: Optional[str] = repeat_expr
        self.repeat_until: Optional[str] = repeat_until

    def _parseContents(self, v: Optional[Any]) -> Optional[bytes]:
        if v is None:
            return None
        match type(v):
            case str():
                return bytes(v)
            case bytes():
                return bytes(v)
            case list():
                return bytes([int(x) for x in v])
        return None

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.id = data["id"]
        self.type = data["type"]
        self.process = data.get("process")
        self.contents = self._parseContents(data.get("contents"))
        self.doc = KaitaiDocAttr(path=self.path)
        self.doc.deserialize(data)  # Same level
        self.size = int(data.get("size"))
        self.size_eos = data.get("size-eos", False)
        self.if_expr = data.get("if")
        self.encoding = data.get("encoding")
        self.terminator = data.get("terminator")
        self.eos_error = data.get("eos-error", True)
        self.pad_right = data.get("pad-right")
        self.enum = data.get("enum")
        self.parent = data.get("parent")
        self.repeat = data.get("repeat")
        self.repeat_expr = data.get("repeat-expr")
        self.repeat_until = data.get("repeat-until")
        self.valid = data.get("valid")

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {"id": self.id}
        if self.type is not None:
            o["type"] = self.type
        if self.process is not None:
            o["process"] = self.process
        if self.contents is not None:
            o["contents"] = [int(x) for x in self.contents]
        o.update(self.doc.serialize())
        if self.size is not None:
            o["size"] = self.size
        if self.size_eos != False:
            o["size-eos"] = self.size_eos
        if self.if_expr is not None:
            o["if"] = self.if_expr
        if self.encoding is not None:
            o["encoding"] = self.encoding
        if self.terminator is not None:
            o["terminator"] = self.terminator
        if self.eos_error != True:
            o["eos-error"] = self.eos_error
        if self.pad_right is not None:
            o["pad-right"] = self.pad_right
        if self.enum is not None:
            o["enum"] = self.enum
        if self.parent is not None:
            o["parent"] = self.parent
        if self.valid is not None:
            o["valid"] = self.valid
        if self.repeat is not None:
            o["repeat"] = self.repeat
        if self.repeat_expr is not None:
            o["repeat-expr"] = self.repeat_expr
        if self.repeat_until is not None:
            o["repeat-until"] = self.repeat_until
        return o
