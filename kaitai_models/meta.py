from ast import List
from enum import Enum
from typing import Any, Dict, Optional

from kaitai_models.serializable import Serializable


class Endianness(Enum):
    BIG = "be"
    LITTLE = "le"


class KaitaiMeta(Serializable):
    def __init__(
        self,
        path: List[str] = [],
        opaque: bool = False,
        id: Optional[str] = None,
        endian: Optional[Endianness] = None,
        bit_endian: Optional[Endianness] = None,
        encoding: Optional[str] = None,
        force_debug: bool = False,
        opaque_types: Optional[bool] = None,
        zero_copy_substream: Optional[bool] = None,
        imports: List[str] = [],
    ) -> None:
        super().__init__(path)
        self.opaque: bool = opaque
        self.id: str = id or ""
        self.endian: Optional[Endianness] = endian
        self.bit_endian: Optional[Endianness] = bit_endian
        self.encoding: Optional[str] = encoding
        self.force_debug: bool = force_debug
        self.opaque_types: Optional[bool] = opaque_types
        self.zero_copy_substream: Optional[bool] = zero_copy_substream
        self.imports: List[str] = imports
        self.extra_shit: Dict[str, Any] = {}

    def deserialize(self, data: Dict[str, Any]) -> None:
        keys = data.keys()
        self.id = data.pop("id")
        if "endian" in keys:
            self.endian = Endianness(data.pop("endian"))
        if "bit-endian" in keys:
            self.bit_endian = Endianness(data.pop("bit-endian"))
        if "encoding" in keys:
            self.encoding = data.pop("encoding")
        if "encoding" in keys:
            self.encoding = data.pop("encoding")
        if "ks-debug" in keys:
            self.force_debug = data.pop("ks-debug")
        if "ks-opaque-types" in keys:
            self.opaque_types = data.pop("ks-opaque-types")
        if "ks-zero-copy-substream" in keys:
            self.zero_copy_substream = data.pop("ks-zero-copy-substream")
        self.extra_shit = data

    def serialize(self) -> Dict[str, Any]:
        o = {"id": self.id}
        if self.endian is not None:
            o["endian"] = self.endian.value
        if self.bit_endian is not None:
            o["bit-endian"] = self.bit_endian.value
        if self.encoding is not None:
            o["encoding"] = self.encoding
        if self.force_debug is not None:
            o["ks-debug"] = self.force_debug
        if self.opaque_types is not None:
            o["ks-opaque_types"] = self.opaque_types
        if self.zero_copy_substream is not None:
            o["ks-zero-copy-substream"] = self.zero_copy_substream
        o.update(self.extra_shit)
        return o
