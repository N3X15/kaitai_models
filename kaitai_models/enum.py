from typing import Any, Dict, List, Optional

from kaitai_models.enum_value import KaitaiEnumValue
from kaitai_models.serializable import Serializable


class KaitaiEnum(Serializable):
    def __init__(
        self,
        path: List[str] = [],
        id: Optional[str] = None,
        members: Dict[int, KaitaiEnumValue] = {},
    ) -> None:
        super().__init__(path)
        self.id: str = id or ""
        self.members: Dict[int, KaitaiEnumValue] = members

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.members = {}
        for k, v in data:
            value = int(k)
            m = KaitaiEnumValue(self.path + [self.id], value)
            m.deserialize(v)
            self.members[value] = m

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {}
        for k, m in sorted(self.members.items()):
            o[str(k)] = m.serialize()
        return o
