from typing import Any, Dict, List


class Serializable:
    def __init__(self, path: List[str] = []) -> None:
        self.path: List[str] = path

    def serialize(self) -> Dict[str, Any]:
        return {}

    def deserialize(self, data: Dict[str, Any]) -> None:
        pass
