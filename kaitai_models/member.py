from typing import List, Optional
from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.serializable import Serializable


class KaitaiMember(Serializable):
    def __init__(
        self,
        path: List[str] = [],
        id: str = "",
        data_type: str = "",
        enum: str = "",
        doc: Optional[KaitaiDocAttr] = None,
    ) -> None:
        super().__init__(path)
        self.id: str = id
        self.data_type: str = data_type
        self.enum: str = enum
        self.doc: KaitaiDocAttr = doc or KaitaiDocAttr(path=self.path)
