from pathlib import Path
from typing import Union

from kaitai_models.class_ import KaitaiClass


def load_kaitai(ksyfile: Union[str, Path]) -> KaitaiClass:
    return KaitaiClass.FromFile(ksyfile)
