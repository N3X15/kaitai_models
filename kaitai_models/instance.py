from typing import Any, Dict, List, Optional

from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.member import KaitaiMember


class KaitaiInstance(KaitaiMember):
    def __init__(
        self,
        path: List[str] = [],
        id: Optional[str] = None,
        type: Optional[str] = None,
        value: Optional[str] = None,
        doc: Optional[KaitaiDocAttr] = None,
        enum: Optional[str] = None,
        if_expr: Optional[str] = None,
        pos: Optional[str] = None,
        io: Optional[str] = None,
        valid: Optional[Any] = None,
    ) -> None:
        super().__init__(path, id, type, enum, doc)
        self.value: Optional[str] = value
        self.if_expr: Optional[str] = if_expr
        self.pos: Optional[str] = pos
        self.io: Optional[str] = io
        self.valid: Optional[Any] = valid

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.type = data["type"]
        self.doc.deserialize(data)

        self.value = data.get("value")
        self.enum = data.get("enum")
        self.if_expr = data.get("if")
        self.pos = data.get("pos")
        self.io = data.get("io")
        self.valid = data.get("valid")

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {}
        if self.type is not None:
            o["type"] = self.type
        o.update(self.doc.serialize())
        if self.value is not None:
            o["value"] = self.value
        if self.enum != False:
            o["enum"] = self.enum
        if self.if_expr is not None:
            o["if"] = self.if_expr
        if self.pos is not None:
            o["pos"] = self.pos
        if self.io is not None:
            o["io"] = self.io
        if self.valid is not None:
            o["valid"] = self.valid
        return o
