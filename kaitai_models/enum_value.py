from typing import Any, Dict, List, Optional

from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.serializable import Serializable


class KaitaiEnumValue(Serializable):
    def __init__(
        self,
        path: List[str] = [],
        value: int = 0,
        name: str = "",
        doc: Optional[KaitaiDocAttr] = None,
    ) -> None:
        super().__init__(path)
        self.id: str = name
        self.value: int = value
        self.doc: KaitaiDocAttr = doc or KaitaiDocAttr(path=self.path)

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.id = data["id"]
        self.doc = KaitaiDocAttr(path=self.path)
        self.doc.deserialize(data)  # Same level

    def serialize(self) -> Dict[str, Any]:
        o = {"id": self.id}
        o.update(self.doc.serialize())
        return o
