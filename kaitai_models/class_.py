from pathlib import Path
from typing import Any, Dict, List, Optional, Union

from ruamel.yaml import YAML as Yaml

from kaitai_models.attr import KaitaiAttr
from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.enum import KaitaiEnum
from kaitai_models.instance import KaitaiInstance
from kaitai_models.meta import KaitaiMeta
from kaitai_models.param import KaitaiParam
from kaitai_models.serializable import Serializable

YAML = Yaml(typ="rt")


class KaitaiClass(Serializable):
    def __init__(
        self,
        filename: Optional[str] = None,
        path: List[str] = [],
        root: bool = True,
        meta: Optional[KaitaiMeta] = None,
        doc: Optional[KaitaiDocAttr] = None,
        to_string_expr: Optional[str] = None,
        params: List[KaitaiParam] = [],
        seq: List[KaitaiAttr] = [],
        types: Dict[str, "KaitaiClass"] = {},
        instances: Dict[str, KaitaiInstance] = {},
        enums: Dict[str, KaitaiEnum] = {},
    ) -> None:
        super().__init__(path)
        self.filename: str = filename
        self.is_root: bool = root
        self.meta: KaitaiMeta = meta or KaitaiMeta(path=self.path + ["meta"])
        self.doc: KaitaiDocAttr = doc or KaitaiDocAttr(path=self.path)
        self.to_string_expr: Optional[str] = to_string_expr
        self.params: List[KaitaiParam] = params
        self.seq: List[KaitaiAttr] = seq
        self.types: Dict[str, "KaitaiClass"] = types
        self.instances: Dict[str, KaitaiInstance] = instances
        self.enums: Dict[str, KaitaiEnum] = enums

    def deserialize(self, data: Dict[str, Any]) -> None:
        keys = data.keys()
        self.meta.path = ["meta"]
        self.meta.deserialize(data["meta"])
        self.doc.deserialize(data)
        self.to_string_expr = data.get("to-string")
        self.params = []
        for i, v in enumerate(data.get("params", [])):
            p = KaitaiParam(["params"], i)
            p.deserialize(v)
            self.params.append(p)
        for i, v in enumerate(data.get("seq", [])):
            s = KaitaiAttr(["seq"], i)
            s.deserialize(v)
            self.seq.append(s)
        if "types" in keys:
            for typeid, typedata in data["types"].items():
                cls = KaitaiClass(self.filename, ["types", typeid], root=False)
                cls.deserialize(typedata)
                self.types[typeid] = cls
        if "instances" in keys:
            for iid, idata in data["instances"].items():
                i = KaitaiInstance(["instances"], iid)
                i.deserialize(idata)
                self.instances[iid] = i
        if "enums" in keys:
            for eid, edata in data["enums"].items():
                e = KaitaiEnum(["enums"], eid)
                e.deserialize(edata)
                self.enums[eid] = e

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {"meta": self.meta.serialize()}
        o.update(self.doc.serialize())
        if self.to_string_expr is not None:
            o["to-string"] = self.to_string_expr
        if len(self.params) > 0:
            o["params"] = [v.serialize() for v in self.params]
        if len(self.seq) > 0:
            o["seq"] = [v.serialize() for v in self.seq]
        if len(self.types) > 0:
            o["types"] = {k: v.serialize() for k, v in sorted(self.types.items())}
        if len(self.instances) > 0:
            o["instances"] = {
                k: v.serialize() for k, v in sorted(self.instances.items())
            }
        if len(self.enums) > 0:
            o["enums"] = {k: v.serialize() for k, v in sorted(self.enums.items())}
        return o

    @staticmethod
    def FromFile(path: Union[str, Path]) -> "KaitaiClass":
        path = Path(path)
        cls = KaitaiClass(
            filename=path.absolute(),
            path=[],
        )
        with path.open("r") as f:
            data = YAML.load(f)
            cls.deserialize(data)
        return cls
