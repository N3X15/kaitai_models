from typing import Any, Dict, List, Optional

from kaitai_models.doc_attr import KaitaiDocAttr
from kaitai_models.member import KaitaiMember


class KaitaiParam(KaitaiMember):
    def __init__(
        self,
        path: List[str] = [],
        id: Optional[str] = None,
        type: Optional[str] = None,
        enum: Optional[str] = None,
        doc: Optional[KaitaiDocAttr] = None,
    ) -> None:
        super().__init__(path, id, type, enum, doc)

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.type = data["type"]
        self.doc.deserialize(data)
        self.enum = data.get("enum")

    def serialize(self) -> Dict[str, Any]:
        o: Dict[str, Any] = {}
        if self.type is not None:
            o["type"] = self.type
        o.update(self.doc.serialize())
        if self.enum is not None:
            o["enum"] = self.enum
        return o
