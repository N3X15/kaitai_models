# Kaitai Models for Python

This repository provides a set of models that represent [Kaitai Struct](https://kaitai.io) *.ksy file structures for the purpose of using the data in these files in Python.

I use these for generating meaningful documentation for a project I work on.

## License

This software is available under the MIT Open Source License.